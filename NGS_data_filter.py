#!/usr/bin/python
# File name : NGS_data_filter.py
#Author: Lei Pinji
#Mail: LPJ@whu.edu.cn
################################
from __future__ import division  
import sys,os,time
import getopt
import re
optlist,args = getopt.getopt(sys.argv[1:],'h',["help","single_end","paired_end"])

########## subroutine ##########
# help_message()    print help message for this pipeline
def help_message():
            print '''##########
Usage:  %s     [options*]   <test.fastq.gz>|<test_R1.fastq.gz test_R2.fastq.gz>
Options:
-h|--help           print this help message
--single_end        single end sequence mapping
--paired_end        paired end sequence mapping

Description:
This script will use [cutadapt] to clipper NGS raw data
1.Remove NGS library adaptor 
2.Remove low quality bases, phred value more than Q20
3.Remove reads contain more than 10 N 
4.Remove reads length less than 20nt
##########''' % sys.argv[0]

def cutadapt_se(fq):
    data = fq
    name = re.compile(r'(.*)\.((fastq.gz|fq.gz))$')
    clean_name = name.sub(r'\1.clean.\2',fq)
    adaptor = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    cmd = 'cutadapt -a %s -q 20,20 --max-n 10 -m 20 -o %s %s 1> NGS.cutadapt.log ' % (adaptor,clean_name,fq)
    print time.strftime(
            "%Y-%m-%d %H:%M:%S"
            )
    print '''Current working process:
    %s
    ''' % cmd
    os.system(cmd)



def cutadapt_pe(fq1,fq2):
    R1 = re.compile(r'(.*R1.*)\.((fastq.gz|fq.gz))$')
    R2 = re.compile(r'(.*R2.*)\.((fastq.gz|fq.gz))$')
    fq1_clean = R1.sub(r'\1.clean.\2',fq1)
    fq2_clean = R2.sub(r'\1.clean.\2',fq2)
    AD3 = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC'
    AD5 = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT'
    cmd = "cutadapt -a %s -A %s -q 20,20 --max-n 10 -m 20 -o %s -p %s %s %s 1> NGS.cutadapt.log" % (AD3,AD5,fq1_clean,fq2_clean,fq1,fq2)
    print time.strftime(
            "%Y-%m-%d %H:%M:%S"
            )
    print '''Current working process:
    %s
    ''' % cmd
    os.system(cmd)

### cutadapt  options
# -a paired end forward adaptor sequence
# -A paired end reverse adaptor sequence
# -q quality trim
# --max-n max number of N base
# -m throw away processed reads shorter than N bases
# -p paired end second file



def q20_30(fq):
    if re.match(r'(.*)\.gz',fq):
        name = re.compile(r'(.*).gz')
        new_name = name.sub(r'\1',fq)
        os.system("gunzip -k %s" % fq)
    file_name = open(new_name,'r')
    q20 = 0
    q30 = 0
    nt = 0
    reads = 0
    for line in file_name:
        title = line
        seq = file_name.next()
        add = file_name.next()
        phred_value = file_name.next()
        reads += 1
        value = 0
        for letter in phred_value.strip("\n"):
            nt += 1
            value = ord(letter)-33
            if (value >=20):
                q20 += 1
            if (value >=30):
                q30 += 1
    q20_ratio = q20/nt
    q30_ratio = q30/nt
    os.remove(new_name)
    return (reads,nt,q20,q20_ratio,q30,q30_ratio)


################################################
output = open("QC_statistics.html",'w')
for opt,value in optlist:
    if opt in ('-h'):
        help_message()
        sys.exit(0)

    if opt in ('--single_end'):
        output = open("QC_statistics.html",'w')
        for fastq in args:
        # raw fastq file name
            output.write('''
<html>
<body>
<p>QC of NGS reads</p>
<table border='1'>
<tr bgcolor='steelblue'>
<td>Raw reads</td>
<td>Raw Bases</td>
<td>Raw Q20 Bases</td>
<td>Raw Q20 Ratio</td>
<td>Raw Q30 Bases</td>
<td>Raw Q30 Ratio</td>
<td>Clean reads</td>
<td>Clean Bases</td>
<td>Clean Q20 Bases</td>
<td>Clean Q20 Ratio</td>
<td>Clean Q30 Bases</td>
<td>Clean Q30 Ratio</td>
</tr>''')
            nt_qs = q20_30(fastq)
            cutadapt_se(fastq)
            name = re.compile(r'(.*)\.((fastq.gz|fq.gz))$')
            clean_name = name.sub(r'\1.clean.\2',fastq)
            nt_qs2 = q20_30(clean_name)
            qc =nt_qs + nt_qs2
            output.write('''
<tr>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
</tr>
</table>
</body>
</html>
''' % qc)

    elif opt in ("--paired_end"):
        output = open("QC_statistics.html",'w')
        fq1 = args[0]
        fq2 = args[1]
        # raw fastq file name
        output.write('''
<html>
<body>
<p>QC of NGS reads</p>
<table border='1'>
<tr bgcolor='steelblue'>
<td>Raw reads</td>
<td>Raw Bases</td>
<td>Raw Q20 Bases</td>
<td>Raw Q20 Ratio</td>
<td>Raw Q30 Bases</td>
<td>Raw Q30 Ratio</td>
<td>Clean reads</td>
<td>Clean Bases</td>
<td>Clean Q20 Bases</td>
<td>Clean Q20 Ratio</td>
<td>Clean Q30 Bases</td>
<td>Clean Q30 Ratio</td>
</tr>
''')
        nt_qs1 = q20_30(fq1)
        nt_qs2 = q20_30(fq2)
        cutadapt_pe(fq1,fq2)
        name1 = re.compile(r'(.*R1.*)\.((fastq.gz|fq.gz))$')
        name2 = re.compile(r'(.*R2.*)\.((fastq.gz|fq.gz))$')
        clean_name1 = name1.sub(r'\1.clean.\2',fq1)
        clean_name2 = name2.sub(r'\1.clean.\2',fq2)
        nt_qs1_clean = q20_30(clean_name1)
        nt_qs2_clean = q20_30(clean_name2)
        qc1 = nt_qs1 + nt_qs1_clean
        qc2 = nt_qs2 + nt_qs2_clean
        qc = qc1 + qc2
        output.write('''
<tr>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
</tr>
<tr>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%d</td>
<td>%d</td>
<td>%f</td>
<td>%d</td>
<td>%f</td>
</tr>
</table>
</body>
''' % qc)









    



        




