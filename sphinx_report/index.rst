.. test documentation master file, created by
   sphinx-quickstart on Thu Aug 11 15:08:51 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ChIP-Seq 数据分析
================================

Contents:

.. toctree::
   :maxdepth: 6

   QC


目录和查找
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

