
ChIP-Seq 测序原始数据质量控制
###############################

测序数据统计结果
--------------------

==== ==== ====
 sam A    B
==== ==== ====
 A   A    A
==== ==== ====


.. list-table:: Weather forecast

    *   - Day
        - Min Temp
        - Max Temp
        -
        - Summary
    *   - Monday
        - 11C
        - 22C
        - 33C
        - A clear day with lots of sunshine
