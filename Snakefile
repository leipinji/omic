configfile:"config.yaml"
rule all:
    input:
        expand("01.raw/QC_q20_q30_{sample}.txt",sample=config['sample']),
        expand("01.raw/{sample}.boxplot.png",sample=config['sample']),
        expand("01.raw/{sample}.barplot.png",sample=config['sample'])
        
##### QC statistic
rule QC:
    input:
        expand("01.raw/{sample}.fastq",sample=config['sample'])
    output: 
        "01.raw/{sample}.quality.stats.txt"
    shell:
        "fastx_quality_stats -i {input} -o {output}"


###### QC phred value converit
rule QC_phred:
    input:
        "01.raw/{sample}.fastq"
    output:
        "01.raw/{sample}.phred.value.txt"
    shell:
        "fastq_quality_converter -n -i {input} -o {output}"

###### QC Q20 and Q30 statistic
rule QC_stat:
    input:
        expand("01.raw/{sample}.phred.value.txt",sample=config['sample'])
    output:
        "01.raw/QC_q20_q30_{sample}.txt"
    shell:
        "./fastq_q20_q30.py {input} > {output}"

###### QC boxplot
rule fastq_quality_boxplot_graph:
    input:
        "01.raw/{sample}.quality.stats.txt"
    output:
        "01.raw/{sample}.boxplot.png"
    shell:
        "fastq_quality_boxplot_graph.sh -i {input} -o {output}"
        
###### QC barplot
rule fastx_nucleotide_distribution_graph:
    input:
        "01.raw/{sample}.quality.stats.txt"
    output:
        "01.raw/{sample}.barplot.png"
    shell:
        "fastx_nucleotide_distribution_graph.sh -i {input} -o {output}"

