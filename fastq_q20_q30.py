#!/usr/bin/python
########################
#
#  fastq_q20_q30.py
#
#  Author: Lei Pinji
#
########################
from __future__ import division
import sys
fastq_stat = sys.argv[1]
fastq = open (fastq_stat,'r')
q20 = 0
q30 = 0
nt = 0
reads = 0
for line in fastq:
    head = line
    seq = fastq.next()
    add = fastq.next()
    phred_value = fastq.next()
    reads += 1
    for value in phred_value.strip("\n").split():
        nt += 1
        if (int(value) >= 20):
            q20 += 1
        if (int(value) >= 30):
            q30 += 1
q20_ratio = q20/nt
q30_ratio = q30/nt
print '''Total reads number is %d
Total nucleotide number is %d
Phred value more than 20 nucleotide number is %d
Phred value more than 30 nucleotide number is %d''' % (reads,nt,q20,q30)



    
